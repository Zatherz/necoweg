local wepgen = { }
local clamp
clamp = function(low, n, high)
  if typelow and high then
    return math.min(math.max(n, low), high)
  elseif low then
    return math.max(n, low)
  elseif high then
    return math.min(n, high)
  end
end
local parseprop
parseprop = function(original, mod)
  local sign = mod[1]
  local value = mod[2]
  local _exp_0 = sign
  if "+" == _exp_0 then
    return original + value
  elseif "-" == _exp_0 then
    return original - value
  elseif "/" == _exp_0 then
    return original / value
  elseif "*" == _exp_0 then
    return original * value
  elseif "=" == _exp_0 then
    return value
  elseif "+%" == _exp_0 then
    return original + (value / 100) * original
  elseif "-%" == _exp_0 then
    return original - (value / 100) * original
  else
    return error("Wrong property sign (" .. (sign or "nil") .. ")")
  end
end
wepgen.constructweapon = function(gensave, mod, base)
  if not mod then
    return "Couldn't construct modifier."
  end
  if not base then
    return "Couldn't construct weapon."
  end
  local newweapon = setmetatable({
    prop = { },
    name = mod .. " " .. base,
    mod = mod,
    base = base
  }, {
    __tostring = function(self)
      local text = {
        "[" .. self.name .. "]"
      }
      for key, value in pairs(self.prop) do
        table.insert(text, key .. " = " .. value)
      end
      return (table.concat(text, "\n"))
    end
  })
  for key, default in pairs(gensave.props.default) do
    local property = gensave.weapons[base][key] or default
    local modifier = gensave.modifiers[mod][key] or {
      "=",
      default
    }
    if modifier == "infinite" or property == "infinite" then
      newweapon.prop[key] = modifier[2] or property
    else
      newweapon.prop[key] = clamp(gensave.props.min[key], parseprop(property, modifier), gensave.props.max[key] == "infinite" and nil)
    end
  end
  return newweapon
end
return setmetatable(wepgen, {
  __call = function(self, gensave, mod, base)
    return wepgen.constructweapon(gensave, mod, base)
  end
})
