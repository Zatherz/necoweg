return {
  ["modifiers"] = {
    ["Sawed-Off"] = {
      spread = {
        "+",
        1
      },
      range = {
        "-",
        1
      }
    },
    ["Unfinished"] = {
      range = {
        "-",
        0.2
      },
      firerate = {
        "-%",
        0.5
      },
      ammo = {
        "+%",
        5
      }
    },
    ["Machine"] = {
      firerate = {
        "+%",
        100
      },
      ammo = {
        "+%",
        50
      }
    },
    ["Short"] = {
      range = {
        "-%",
        50
      },
      damage = {
        "+%",
        20
      },
      ammo = {
        "+%",
        10
      }
    },
    ["Super"] = {
      range = {
        "-%",
        10
      },
      damage = {
        "+%",
        100
      },
      ammo = {
        "-%",
        50
      }
    },
    ["Ultra"] = {
      damage = {
        "+%",
        500
      },
      ammo = {
        "-%",
        80
      }
    },
    ["Vanilla"] = { }
  },
  ["weapons"] = {
    ["Shotgun"] = {
      spread = 4,
      range = 4,
      firerate = 2,
      damage = 1.2,
      ammo = 100
    },
    ["Machinegun"] = {
      spread = 1.2,
      range = 6,
      firerate = 6,
      ammo = 200
    },
    ["AWP"] = {
      spread = 1,
      firerate = 5,
      range = "infinite",
      damage = 4,
      ammo = 25
    },
    ["Sniper Rifle"] = {
      spread = 1.5,
      firerate = 2,
      range = "infinite",
      damage = 5,
      ammo = 30
    },
    ["RPG"] = {
      spread = 1.2,
      range = 15,
      damage = 15,
      firerate = 1,
      ammo = 15
    },
    ["Basic"] = { }
  },
  ["props"] = {
    default = {
      range = 1,
      spread = 1,
      firerate = 2,
      damage = 2,
      ammo = 100
    },
    min = {
      range = 1,
      spread = 1,
      firerate = 1,
      damage = 1,
      ammo = 1
    },
    max = {
      range = 10,
      spread = 20,
      firerate = 15,
      damage = nil,
      ammo = nil
    }
  }
}
