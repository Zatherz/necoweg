local commands = require("commands")
local lastcommand = nil
local runcommand
runcommand = function(input)
  local argtable = { }
  for argument in input:gmatch("([^:]+)") do
    local formattedarg = string.gsub(argument, ":", "")
    table.insert(argtable, formattedarg)
  end
  if argtable[1] == "last" or argtable[1] == "!" then
    if not lastcommand then
      print("There is no last command.")
      return 
    else
      return (runcommand(lastcommand))
    end
  elseif commands[argtable[1]] then
    local justargs = { }
    for i = 2, #argtable do
      table.insert(justargs, argtable[i])
    end
    commands[argtable[1]](justargs)
  else
    print("Error: command '" .. (argtable[1] or "(nil)") .. "' doesn't exist.")
  end
  lastcommand = input
end
local file = arg[1]
local nosave = arg[2]
if file then
  print("File reading mode. Reading file '" .. file .. "'.")
  local filehandler = io.open(file, "r")
  if not filehandler then
    return print("File '" .. file .. "' couldn't be opened.")
  else
    for line in io.lines(file) do
      if not pcall(runcommand, line) then
        print("Error happened! Please report this stacktrace:")
        print(debug.traceback())
      end
    end
    filehandler:close()
    if not nosave then
      if not pcall(runcommand, "save") then
        print("Error happened! Please report this stacktrace:")
        return print(debug.traceback())
      end
    end
  end
else
  print("Interactive mode. Use 'help' for information.")
  while (true) do
    io.write("> ")
    io.flush()
    runcommand(io.stdin:read())
  end
end
