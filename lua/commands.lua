local version = require("version")
local weapons = {
  weapons = { },
  modifiers = { },
  props = { }
}
local wepgen = require("wepgen")
local props = weapons.props
local serpent = require("serpent")
local moonscript = require("moonscript.base")
math.randomseed(os.time())
table.keychoice = function(tab)
  local keys = { }
  local index = 1
  for key, _ in pairs(tab) do
    table.insert(keys, key)
    index = index + 1
  end
  if #keys < 1 then
    return print("Table is empty.")
  else
    return keys[math.random(1, #keys)]
  end
end
local helpdocs = {
  {
    command = "help",
    args = "[command]",
    desc = "Print help info."
  },
  {
    command = "version",
    args = "",
    desc = "Print the version of this program."
  },
  {
    command = "echo",
    args = "[text:[text...]]",
    desc = "Print the arguments."
  },
  {
    command = "exit",
    args = "",
    desc = "Exit the program"
  },
  {
    command = "weapon",
    args = "[modifier:[weapons]]",
    desc = "Generate a weapon. 'modifier' and 'weapons' accept a special value 'random'."
  },
  {
    command = "weapon.add",
    args = "id",
    desc = "Create a weapon."
  },
  {
    command = "weapon.remove",
    args = "id",
    desc = "Remove a weapon."
  },
  {
    command = "weapon.set",
    args = "id:prop[:value]",
    desc = "Set a property of the weapon. If 'value' is not passed, the property will be set to its default value."
  },
  {
    command = "weapon.get",
    args = "[id[:prop]]",
    desc = "Print the property of a weapon. If 'prop' is not passed, all the properties will be listed. If no arguments are passed, all the weapons will be listed."
  },
  {
    command = "weapon.clear",
    args = "",
    desc = "Remove all weapons."
  },
  {
    command = "modifier.add",
    args = "id",
    desc = "Create a modifier."
  },
  {
    command = "modifier.remove",
    args = "id",
    desc = "Remove a modifier."
  },
  {
    command = "modifier.set",
    args = "id:prop[:sign[:value]]",
    desc = "Set a property of the modifier. If 'value' is not passed, the property will be set to its default value with the chosen sign. If neither 'sign' nor 'value' are passed, the property will be set to the default value with the = sign."
  },
  {
    command = "modifier.get",
    args = "[id[:prop]]",
    desc = "Print the property of a modifier. If 'prop' is not passed, all the properties will be listed. If no arguments are passed, all the modifiers will be listed."
  },
  {
    command = "modifier.clear",
    args = "",
    desc = "Remove all modifiers."
  },
  {
    command = "prop.set",
    args = "prop:default:min[:max]",
    desc = "Set a global property. If 'max' not passed, max will be set to a special value 'infinite'"
  },
  {
    command = "prop.remove",
    args = "prop",
    desc = "Remove global property data."
  },
  {
    command = "prop.get",
    args = "prop[:type]",
    desc = "Print global property data. The 'type' argument has to be 'default', 'min' or 'max', or not present. If not present, all three will be displayed at once."
  },
  {
    command = "prop.clear",
    args = "",
    desc = "Remove all global properties."
  }
}
local commands = { }
commands = {
  version = function(args)
    return print(version)
  end,
  echo = function(args)
    return print(table.concat(args, " "))
  end,
  weapon = function(args)
    local mod = table.keychoice(weapons.modifiers)
    local base = table.keychoice(weapons.weapons)
    if #args > 0 then
      if args[1] == "random" then
        mod = table.keychoice(weapons.modifiers)
      elseif weapons.modifiers[args[1]] then
        mod = args[1]
      else
        print("Modifier '" .. (args[1] or "(nil)") .. "' is not implemented.")
        return 
      end
    end
    if #args > 1 then
      if args[2] == "random" then
        base = table.keychoice(weapons.weapons)
      elseif weapons.weapons[args[2]] then
        base = args[2]
      else
        print("Weapon '" .. (args[2] or "(nil)") .. "' is not implemented.")
        return 
      end
    end
    return print(wepgen(weapons, mod, base))
  end,
  help = function(args)
    if #args == 0 then
      for index, helpdoc in ipairs(helpdocs) do
        print(helpdoc.command or "[undefined]")
        print("\t\t" .. helpdoc.desc or "undefined")
      end
    else
      local command = nil
      for index, helpdoc in ipairs(helpdocs) do
        if helpdoc.command == args[1] then
          command = helpdoc
          break
        end
      end
      if not command then
        if not commands[command] then
          return print("Command '" .. (args[1] or "nil") .. "' doesn't exist.")
        else
          return print("Command '" .. (args[1] or "nil") .. "' doesn't have a help doc.")
        end
      else
        print("Usage: " .. command.command .. " " .. command.args)
        return print("Description: " .. command.desc)
      end
    end
  end,
  exit = function()
    return os.exit()
  end,
  ["weapon.add"] = function(args)
    if #args > 0 then
      if weapons.weapons[args[1]] then
        return print("Weapon " .. (args[1] or "nil") .. " exists.")
      else
        weapons.weapons[args[1]] = { }
      end
    else
      return print("Minimum 1 argument.")
    end
  end,
  ["weapon.remove"] = function(args)
    if #args > 0 then
      if not weapons.weapons[args[1]] then
        return print("Weapon " .. (args[1] or "nil") .. " doesn't exist.")
      else
        weapons.weapons[args[1]] = nil
      end
    else
      return print("Minimum 1 argument.")
    end
  end,
  ["weapon.set"] = function(args)
    if #args > 1 then
      if not args[2] or not weapons.weapons[args[1]] then
        return print("Weapon " .. (args[1] or "nil") .. " doesn't exist.")
      elseif not args[2] or not props.default[args[2]] then
        return print("Property " .. (args[2] or "(nil)") .. " doesn't exist.")
      else
        weapons.weapons[args[1]][args[2]] = args[3] or props.default[args[2]]
      end
    else
      return print("Minimum 2 arguments")
    end
  end,
  ["weapon.get"] = function(args)
    if #args == 0 then
      for key, _ in pairs(weapons.weapons) do
        print(key)
      end
    elseif #args > 0 then
      if not args[1] or not weapons.weapons[args[1]] then
        return print("Weapon " .. (args[1] or "nil") .. " doesn't exist.")
      elseif #args > 1 then
        if not args[2] or not props.default[args[2]] then
          print("Property " .. (args[2] or "(nil)") .. " doesn't exist.")
        end
        return print(weapons.weapons[args[1]][args[2]] or props.default[args[2]])
      else
        for key, default in pairs(props.default) do
          print(key .. " = " .. (weapons.weapons[args[1]][key] or default))
        end
      end
    end
  end,
  ["weapon.clear"] = function(args)
    weapons.weapons = { }
  end,
  ["modifier.add"] = function(args)
    if #args > 0 then
      if weapons.modifiers[args[1]] then
        return print("Modifier " .. (args[1] or "nil") .. " exists.")
      else
        weapons.modifiers[args[1]] = { }
      end
    else
      return print("Minimum 1 argument.")
    end
  end,
  ["modifier.remove"] = function(args)
    if #args > 0 then
      if not weapons.modifiers[args[1]] then
        return print("Modifier " .. (args[1] or "nil") .. " doesn't exist.")
      else
        weapons.modifiers[args[1]] = nil
      end
    else
      return print("Minimum 1 argument.")
    end
  end,
  ["modifier.set"] = function(args)
    if #args > 1 then
      local validsigns = {
        ["+"] = true,
        ["-"] = true,
        ["="] = true,
        ["+%"] = true,
        ["-%"] = true,
        ["*"] = true,
        ["/"] = true
      }
      if not args[1] or not weapons.modifiers[args[1]] then
        return print("Modifier " .. (args[1] or "nil") .. " doesn't exist.")
      elseif not args[2] or not props.default[args[2]] then
        return print("Property " .. (args[2] or "(nil)") .. " doesn't exist.")
      elseif not validsigns[args[3] or "="] then
        return print("Sign '" .. (args[3] or "(nil)") .. "' doesn't exist.")
      else
        local num = tonumber(args[4] or props.default[args[2]])
        if not num then
          print("Argument #4 has to be a number")
        end
        weapons.modifiers[args[1]][args[2]] = {
          args[3] or "=",
          num
        }
      end
    else
      return print("Minimum 2 arguments")
    end
  end,
  ["modifier.get"] = function(args)
    if #args == 0 then
      for key, _ in pairs(weapons.modifiers) do
        print(key)
      end
    elseif #args > 0 then
      if not args[1] or not weapons.modifiers[args[1]] then
        print("Modifier " .. (args[1] or "nil") .. " doesn't exist.")
      end
      if #args > 1 then
        if not args[2] or not props.default[args[2]] then
          print("Property " .. (args[2] or "(nil)") .. " doesn't exist.")
        end
        local mod = weapons.modifiers[args[1]][args[2]] or {
          "=",
          props.default[args[2]]
        }
        return print(mod[1] .. mod[2])
      else
        for key, default in pairs(props.default) do
          local mod = weapons.modifiers[args[1]][key] or {
            "=",
            default
          }
          print(key .. " = " .. mod[1] .. mod[2])
        end
      end
    end
  end,
  ["modifier.clear"] = function(args)
    weapons.modifiers = { }
  end,
  ["prop.remove"] = function(args)
    if #args > 0 then
      if not props.default[args[1]] then
        return print("Property '" .. (args[1] or "nil") .. "' doesn't exist.")
      else
        props.default[args[1]] = nil
        props.min[args[1]] = nil
        props.max[args[1]] = nil
      end
    else
      return print("Minimum 1 argument.")
    end
  end,
  ["prop.set"] = function(args)
    if #args > 2 then
      local default = tonumber(args[2])
      local min = tonumber(args[3])
      local max = tonumber(args[4]) or "infinite"
      if not default or not min then
        return print("Arguments #2-3 must be numbers.")
      else
        if default < min or (type(max) == "number" and (default > max)) then
          return print("Default value is not between min and max values.")
        elseif type(max) == "number" and (min > max) then
          return print("Minimum value is bigger than the maximum value.")
        elseif type(max) == "number" and (max < min) then
          return print("Maximum value is smaller than the minimum value.")
        else
          props.default[args[1]] = default
          props.min[args[1]] = min
          props.max[args[1]] = max
        end
      end
    else
      return print("Minimum 3 arguments.")
    end
  end,
  ["prop.get"] = function(args)
    if #args == 0 then
      if props.default then
        for key, default in pairs(props.default) do
          print(key .. " = default:" .. default .. ", max:" .. (props.max[key] or "infinite") .. ", min:" .. props.min[key])
        end
      end
    else
      if not props.default[args[1]] then
        return print("Property '" .. (args[1] or "nil") .. "' doesn't exist.")
      else
        if #args > 1 then
          if not ({
            default = true,
            min = true,
            max = true
          })[args[2]] then
            return print("Second argument must be 'default', 'min' or 'max'.")
          else
            return print(props[args[2]][args[1]])
          end
        else
          print("default = " .. props.default[args[1]])
          print("min = " .. props.min[args[1]])
          return print("max = " .. props.max[args[1]])
        end
      end
    end
  end,
  ["prop.clear"] = function(args)
    props.default = { }
    props.min = { }
    props.max = { }
  end,
  save = function(args)
    local data = "-- Generated by NeCoWeG, don't edit by hand\nreturn " .. serpent.block(weapons, {
      comment = false
    })
    if #args > 0 then
      local savefile = io.open(args[1], "w")
      if not savefile then
        return print("Couldn't open file.")
      else
        local success = savefile:write(data)
        savefile:close()
        if success then
          return print("Saved file '" .. (args[1] or "nil") .. "'.")
        else
          return print("Failed saving file.")
        end
      end
    else
      return print(data)
    end
  end,
  load = function(args)
    if #args > 0 then
      local filehandle = io.open(args[1], "r")
      if not filehandle then
        return print("Couldn't open file.")
      else
        local data = filehandle:read("*a")
        local success, newweapons = serpent.load(data)
        if not success then
          print("Couldn't compile file.")
        else
          weapons.modifiers = newweapons.modifiers
          weapons.weapons = newweapons.weapons
          weapons.props = newweapons.props
          props = weapons.props
          print("Loaded data")
        end
        return filehandle:close()
      end
    else
      return print("Minimum 1 argument.")
    end
  end
}
table.sort(commands, function(a, b) end)
return (commands)
