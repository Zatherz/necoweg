version = require("version")
weapons = {weapons: {}, modifiers: {}, props: {}}
wepgen = require("wepgen")
props = weapons.props
-- originally props was loaded form a separate file
-- but this makes it simpler to save so I decided to just put it in weapons.moon
serpent = require("serpent")
moonscript = require("moonscript.base")

math.randomseed(os.time())

table.keychoice = (tab) ->
  keys = {}
  index = 1
  for key, _ in pairs(tab) do
    table.insert(keys, key)
    index += 1
  if #keys < 1
    print("Table is empty.")
  else
    return keys[math.random(1, #keys)]

helpdocs = {
  {
    command: "help"
    args: "[command]"
    desc: "Print help info."
  }
  {
    command: "version"
    args: ""
    desc: "Print the version of this program."
  }
  {
    command: "echo"
    args: "[text:[text...]]"
    desc: "Print the arguments."
  }
  {
    command: "exit"
    args: ""
    desc: "Exit the program"
  }
  {
    command: "weapon"
    args: "[modifier:[weapons]]"
    desc: "Generate a weapon. 'modifier' and 'weapons' accept a special value 'random'."
  }
  {
    command: "weapon.add"
    args: "id"
    desc: "Create a weapon."
  }
  {
    command: "weapon.remove"
    args: "id"
    desc: "Remove a weapon."
  }
  {
    command: "weapon.set"
    args: "id:prop[:value]"
    desc: "Set a property of the weapon. If 'value' is not passed, the property will be set to its default value."
  }
  {
    command: "weapon.get"
    args: "[id[:prop]]"
    desc: "Print the property of a weapon. If 'prop' is not passed, all the properties will be listed. If no arguments are passed, all the weapons will be listed."
  }
  {
    command: "weapon.clear"
    args: ""
    desc: "Remove all weapons."
  }
  {
    command: "modifier.add"
    args: "id"
    desc: "Create a modifier."
  }
  {
    command: "modifier.remove"
    args: "id"
    desc: "Remove a modifier."
  }
  {
    command: "modifier.set"
    args: "id:prop[:sign[:value]]"
    desc: "Set a property of the modifier. If 'value' is not passed, the property will be set to its default value with the chosen sign. If neither 'sign' nor 'value' are passed, the property will be set to the default value with the = sign. Possible signs: '+' (add), '-' (subtract), '+%' (add percent), '-%' (subtract percent), '/' (divide), '*' (multiply), '=' (set) and '|' (or, value set by weapon if existing or this one)."
  }
  {
    command: "modifier.get"
    args: "[id[:prop]]"
    desc: "Print the property of a modifier. If 'prop' is not passed, all the properties will be listed. If no arguments are passed, all the modifiers will be listed."
  }
  {
    command: "modifier.clear"
    args: ""
    desc: "Remove all modifiers."
  }
  {
    command: "prop.set"
    args: "prop:default:min[:max]"
    desc: "Set a global property. If 'max' not passed, max will be set to a special value 'infinite'"
  }
  {
    command: "prop.remove"
    args: "prop"
    desc: "Remove global property data."
  }
  {
    command: "prop.get"
    args: "prop[:type]"
    desc: "Print global property data. The 'type' argument has to be 'default', 'min' or 'max', or not present. If not present, all three will be displayed at once."
  }
  {
    command: "prop.clear"
    args: ""
    desc: "Remove all global properties."
  }
}

commands = {}
commands = {
  version: (args) ->
    print(version)
  echo: (args) ->
    print(table.concat(args, " "))
  weapon: (args) ->
    mod = table.keychoice(weapons.modifiers)
    base = table.keychoice(weapons.weapons)

    if #args > 0
      if args[1] == "random"
        mod = table.keychoice(weapons.modifiers)
      elseif weapons.modifiers[args[1]]
        mod = args[1]
      else
        print("Modifier '" .. (args[1] or "(nil)") .. "' is not implemented.")
        return
    if #args > 1
      if args[2] == "random"
        base = table.keychoice(weapons.weapons)
      elseif weapons.weapons[args[2]]
        base = args[2]
      else
        print("Weapon '" .. (args[2] or "(nil)") .. "' is not implemented.")
        return
    
    print(wepgen(weapons, mod, base))
  help: (args) ->
    if #args == 0
      for index, helpdoc in ipairs(helpdocs)
        print(helpdoc.command or "[undefined]")
        print("\t\t" .. helpdoc.desc or "undefined")
    else
      command = nil --
      for index, helpdoc in ipairs(helpdocs)
        if helpdoc.command == args[1]
          command = helpdoc
          break
      if not command then
        if not commands[command]
          print("Command '" .. (args[1] or "nil") .. "' doesn't exist.")
        else
          print("Command '" .. (args[1] or "nil") .. "' doesn't have a help doc.")
      else
        print("Usage: " .. command.command .. " " .. command.args)
        print("Description: " .. command.desc)
    
  exit: ->
    os.exit()
    
  "weapon.add": (args) ->
    if #args > 0
      if weapons.weapons[args[1]]
        print("Weapon " .. (args[1] or "nil") .. " exists.")
      else
        weapons.weapons[args[1]] = {}
    else
      print("Minimum 1 argument.")
      
  "weapon.remove": (args) ->
    if #args > 0
      if not weapons.weapons[args[1]]
        print("Weapon " .. (args[1] or "nil") .. " doesn't exist.")
      else
        weapons.weapons[args[1]] = nil --
    else
      print("Minimum 1 argument.")
  "weapon.set": (args) ->
    if #args > 1
      if not args[2] or not weapons.weapons[args[1]]
        print("Weapon " .. (args[1] or "nil") .. " doesn't exist.")
      elseif not args[2] or not props.default[args[2]]
        print("Property " .. (args[2] or "(nil)") .. " doesn't exist.")
      else
        weapons.weapons[args[1]][args[2]] = args[3] or props.default[args[2]]
    else
      print("Minimum 2 arguments")
  "weapon.get": (args) ->
    if #args == 0
      for key, _ in pairs(weapons.weapons) do
          print(key)
    elseif #args > 0
      if not args[1] or not weapons.weapons[args[1]]
        print("Weapon " .. (args[1] or "nil") .. " doesn't exist.")
      elseif #args > 1
        if not args[2] or not props.default[args[2]]
          print("Property " .. (args[2] or "(nil)") .. " doesn't exist.")
        print(weapons.weapons[args[1]][args[2]] or props.default[args[2]])
      else
        for key, default in pairs(props.default)
          print(key .. " = " .. (weapons.weapons[args[1]][key] or default))
  "weapon.clear": (args) ->
    weapons.weapons = {}
  "modifier.add": (args) ->
    if #args > 0
      if weapons.modifiers[args[1]]
        print("Modifier " .. (args[1] or "nil") .. " exists.")
      else
        weapons.modifiers[args[1]] = {}
    else
      print("Minimum 1 argument.")
      
  "modifier.remove": (args) ->
    if #args > 0
      if not weapons.modifiers[args[1]]
        print("Modifier " .. (args[1] or "nil") .. " doesn't exist.")
      else
        weapons.modifiers[args[1]] = nil --
    else
      print("Minimum 1 argument.")
  "modifier.set": (args) ->
    if #args > 1
      validsigns = {"+":true,"-":true,"=":true,"+%":true,"-%":true,"*":true,"/":true}
      if not args[1] or not weapons.modifiers[args[1]]
        print("Modifier " .. (args[1] or "nil") .. " doesn't exist.")
      elseif not args[2] or not props.default[args[2]]
        print("Property " .. (args[2] or "(nil)") .. " doesn't exist.")
      elseif not validsigns[args[3] or "="]
        print("Sign '" .. (args[3] or "(nil)") .. "' doesn't exist.")
      else
        num = tonumber(args[4] or props.default[args[2]])
        if not num then
          print("Argument #4 has to be a number")
        weapons.modifiers[args[1]][args[2]] = {args[3] or "=", num}
    else
      print("Minimum 2 arguments")
  "modifier.get": (args) ->
    if #args == 0
      for key, _ in pairs(weapons.modifiers) do
        print(key)
    elseif #args > 0
      if not args[1] or not weapons.modifiers[args[1]]
        print("Modifier " .. (args[1] or "nil") .. " doesn't exist.")
      if #args > 1
        if not args[2] or not props.default[args[2]]
          print("Property " .. (args[2] or "(nil)") .. " doesn't exist.")
        mod = weapons.modifiers[args[1]][args[2]] or {"=", props.default[args[2]]}
        print(mod[1] .. mod[2])
      else
        for key, default in pairs(props.default)
          mod = weapons.modifiers[args[1]][key] or {"=", default}
          print(key .. " = " .. mod[1] .. mod[2])
  "modifier.clear": (args) ->
    weapons.modifiers = {}
  "prop.remove": (args) ->
    if #args > 0
      if not props.default[args[1]]
        print("Property '" .. (args[1] or "nil") .. "' doesn't exist.")
      else
        props.default[args[1]] = nil --
        props.min[args[1]] = nil --
        props.max[args[1]] = nil --
    else
      print("Minimum 1 argument.")
  "prop.set": (args) ->
    if #args > 2
      default = tonumber(args[2])
      min = tonumber(args[3])
      max = tonumber(args[4]) or "infinite"
      if not default or not min
        print("Arguments #2-3 must be numbers.")
      else
        if default < min or (type(max) == "number" and (default > max))
          print("Default value is not between min and max values.")
        elseif type(max) == "number" and (min > max)
          print("Minimum value is bigger than the maximum value.")
        elseif type(max) == "number" and (max < min)
          print("Maximum value is smaller than the minimum value.")
        else
          props.default = props.default or {}
          props.min = props.min or {}
          props.max = props.max or {}
          
          props.default[args[1]] = default
          props.min[args[1]] = min
          props.max[args[1]] = max
    else
      print("Minimum 3 arguments.")
  "prop.get": (args) ->
    if #args == 0
      if props.default
        props.default = props.default or {}
        for key, default in pairs(props.default) do
          print(key .. " = default:" .. default .. ", max:" .. (props.max[key] or "infinite") .. ", min:" .. props.min[key])
    else
      if not props.default[args[1]]
        print("Property '" .. (args[1] or "nil") .. "' doesn't exist.")
      else
        if #args > 1
          if not ({default:true, min:true, max:true})[args[2]]
            print("Second argument must be 'default', 'min' or 'max'.")
          else
            print(props[args[2]][args[1]])
        else
          print("default = " .. props.default[args[1]])
          print("min = " .. props.min[args[1]])
          print("max = " .. props.max[args[1]])
  "prop.clear": (args) ->
    props.default = {}
    props.min = {}
    props.max = {}
  save: (args) ->
    data = "-- Generated by NeCoWeG, don't edit by hand\nreturn " .. serpent.block(weapons, {comment: false})
    if #args > 0
      savefile = io.open(args[1], "w")
      if not savefile
        print("Couldn't open file.")
      else
        success = savefile\write(data)
        savefile\close()
        if success
          print("Saved file '" .. (args[1] or "nil") .. "'.")
        else
          print("Failed saving file.")
    else
      print(data)
  load: (args) ->
    if #args > 0
      filehandle = io.open(args[1], "r")
      if not filehandle
        print("Couldn't open file.")
      else
        data = filehandle\read("*a")
        success, newweapons = serpent.load(data)
        if not success
          print("Couldn't compile file.")
        else
          weapons.modifiers = newweapons.modifiers
          weapons.weapons = newweapons.weapons
          weapons.props = newweapons.props
          props = weapons.props
          print("Loaded data")
        filehandle\close()
    else
      print("Minimum 1 argument.")
}
table.sort(commands, (a, b) ->
  
)

return(commands)
