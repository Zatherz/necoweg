# Necoweg - Needlessly Complex Weapon Generator

This is a Lua/Moonscript module for generating weapon names and their properties.

Documentation is pretty much non-existant right now, I'll try to add it soon.

Run `lua/necoweg.lua` (with Lua) or `necoweg.moon` (with MoonScript) to try out the interactive mode. Add a file argument to scan the file line by line and then dump the save table at the end. Put anything as the second parameter to avoid dumping the save at the end.

Run the `load:weapons.lua` command to load a bunch of presets. View them with `weapon.get`, `modifier.get` and `prop.get`.

Run the `help` command in interactive mode to get a list of all commands. Run `help:NAMEOFCOMMAND` to get specific help for `NAMEOFCOMMAND`.

Warning: Arguments are separated **by colons** (`:`), not by whitespace.

Report any bugs on the bug tracker (see the sidebar on the left).
