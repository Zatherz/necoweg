wepgen = {}

clamp = (low, n, high) -> 
  if low and high
    math.min(math.max(n, low), high)
  elseif low
    math.max(n, low)
  elseif high
    math.min(n, high)

parseprop = (original, mod) ->
  sign = mod[1]
  value = mod[2]
  switch sign
    when "+"
      return original + value
    when "-"
      return original - value
    when "/"
      return original / value
    when "*"
      return original * value
    when "="
      return value
    when "|"
      return original or value
    when "+%"
      return original + (value / 100) * original
    when "-%"
      return original - (value / 100) * original
    else
      error("Wrong property sign (" .. (sign or "nil") .. ")")

wepgen.constructweapon = (gensave, mod, base) ->
  if not mod
    return "Couldn't construct modifier."
  if not base
    return "Couldn't construct weapon."
  newweapon = setmetatable({
    prop: {}
    name: mod .. " " .. base
    mod: mod
    base: base
  }, {
    __tostring: =>
      text = {"[" .. self.name .. "]"}
      for key, value in pairs(self.prop) do
        table.insert(text, key .. " = " .. value)
      return (table.concat(text, "\n"))
  })
  
  gensave.props.default = gensave.props.default or {}
  for key, default in pairs(gensave.props.default) do
    property = gensave.weapons[base][key] or default
    modifier = gensave.modifiers[mod][key] or {"|", default}
    if modifier == "infinite" or property == "infinite"
      newweapon.prop[key] = modifier[2] or property
    else
      newweapon.prop[key] = clamp(gensave.props.min[key], parseprop(property, modifier), gensave.props.max[key] == "infinite" and nil)
  return newweapon
  
return setmetatable(wepgen, {__call: (self, gensave, mod, base) ->
  wepgen.constructweapon(gensave, mod, base)
})