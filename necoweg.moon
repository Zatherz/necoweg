commands = require("commands")

lastcommand = nil --

runcommand = (input) ->
  argtable = {}
  for argument in input\gmatch("([^:]+)") do -- split on colons
    formattedarg = string.gsub(argument, ":", "")
    table.insert(argtable, formattedarg)
  
  if argtable[1] == "last" or argtable[1] == "!" then
    if not lastcommand then
      print("There is no last command.")
      return
    else
      return(runcommand(lastcommand))
  elseif commands[argtable[1]] then
    justargs = {}
    for i = 2, #argtable do
      table.insert(justargs, argtable[i])
    commands[argtable[1]](justargs)
  else
    print("Error: command '" .. (argtable[1] or "(nil)") .. "' doesn't exist.")
    
  lastcommand = input

file = arg[1]
nosave = arg[2]

if file
  print("File reading mode. Reading file '" .. file .. "'.")
  filehandler = io.open(file, "r")
  if not filehandler
    print("File '" .. file .. "' couldn't be opened.")
  else
    for line in io.lines(file)
      if not pcall(runcommand, line)
        print("Error happened! Please report this stacktrace:")
        print(debug.traceback())
    filehandler\close()
    if not nosave
      if not pcall(runcommand, "save")
        print("Error happened! Please report this stacktrace:")
        print(debug.traceback())
else
  print("Interactive mode. Use 'help' for information.")
  while (true)
    io.write("> ")
    io.flush()
    runcommand(io.stdin\read())
    --print("Error happened! Please report this stacktrace:")
    --print(debug.traceback())